package com.clusus.springboot.service;

import com.clusus.springboot.model.Deal;
import com.clusus.springboot.repository.DealRepository;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by anuj on 8/17/22.
 */
@Service
public class DealService {

    Logger log= LoggerFactory.getLogger(DealService.class);

    @Autowired
    public DealRepository dealRepository;

    public List<Deal> getDeals(){
        return dealRepository.findAll();
    }

    public Boolean existsByUniqueId(String uniqueId){
        return dealRepository.existsByUniqueId(uniqueId);
    }

    public boolean isValidUniqueId(String id) {


        return (id != null && !dealRepository.existsByUniqueId(id));

    }

    public boolean isValidDate(String timestamp) {

        try {

            new Date(Long.parseLong(timestamp));
            return true;

        } catch (Exception e) {

            e.printStackTrace();
            log.error("Date parse error");
            return false;
        }

    }

    public boolean isValidAmount(String amount) {
        try {
            Double.parseDouble(amount);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Double parse error");
            return false;
        }
    }

    public String getFromCurrencyISOCode(JSONObject json) throws JSONException {

        return (json.get("fromCurrencyISOCode") == null ? null : json.get("fromCurrencyISOCode").toString());
    }

    public String getToCurrencyISOCode(JSONObject json)throws JSONException{
        return (json.get("toCurrencyISOCode") == null ? null : json.get("toCurrencyISOCode").toString());
    }

    public ResponseEntity<String> saveDeal(JSONObject json) throws JSONException{

        String uniqueId = json.get("uniqueId").toString();
        if (!isValidUniqueId(uniqueId)) {
            return new ResponseEntity<String>("Duplicate or missing unique Id", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String timestamp = json.get("timestamp").toString();
        if (!isValidDate(timestamp)) {
            return new ResponseEntity<String>("Invalid date", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String amount = json.get("amount").toString();
        if (!isValidAmount(amount)) {
            return new ResponseEntity<String>("Invalid amount", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String fromCurrencyISOCode=getFromCurrencyISOCode(json);

        String toCurrencyISOCode=getToCurrencyISOCode(json);

        Deal deal = new Deal();
        deal.setUniqueId(uniqueId);
        deal.setFromCurrencyISOCode(fromCurrencyISOCode);
        deal.setToCurrencyISOCode(toCurrencyISOCode);
        deal.setTimestamp(new Date(Long.parseLong(timestamp)));
        deal.setAmount(Double.parseDouble(amount));
        saveDealToDatabase(deal);


        return new ResponseEntity<String>("Success", HttpStatus.OK);
    }

    public void saveDealToDatabase(Deal deal){

        System.out.println("dealRepository = " + dealRepository);

        dealRepository.save(deal);
    }
}
