package com.clusus.springboot.repository;

import com.clusus.springboot.model.Deal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by anuj on 12/16/22.
 */
@Repository
public interface DealRepository extends JpaRepository<Deal, Integer> {


    Boolean existsByUniqueId(String uniqueId);

 }