package com.clusus.springboot.controller;

import com.clusus.springboot.model.Deal;
import com.clusus.springboot.service.DealService;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * Created by anuj on 12/16/22.
 */


@RestController
@RequestMapping("/api/v1/deal")
public class DealController {

    Logger log=LoggerFactory.getLogger(DealController.class);

    @Autowired
    public DealService dealService;

    @GetMapping
    public List<Deal> getAllDeals() {
        return dealService.getDeals();
    }

    // build create employee REST API
    @PostMapping
    public ResponseEntity<String> createDeal(@RequestBody String request) {

        try {

            JSONObject json = new JSONObject(request);
            return dealService.saveDeal(json);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            return new ResponseEntity<String>("Error processing the request", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
