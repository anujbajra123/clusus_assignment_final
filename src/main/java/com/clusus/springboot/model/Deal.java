package com.clusus.springboot.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by anuj on 12/16/22.
 */

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "deal")
public class Deal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "unique_id",unique=true, nullable = false)
    private String uniqueId;

    @Column(name = "from_currency_ISO_code")
    private String fromCurrencyISOCode;

    @Column(name = "to_currency_ISO_code")
    private String toCurrencyISOCode;

    @Column(name = "timestamp")
    private Date timestamp;

    @Column(name = "amount")
    private Double amount;

    public Integer getId() {
        return id;
    }

    public String getFromCurrencyISOCode() {
        return fromCurrencyISOCode;
    }

    public String getToCurrencyISOCode() {
        return toCurrencyISOCode;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public Double getAmount() {
        return amount;
    }

    public String getUniqueId() {
        return uniqueId;
    }


    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public void setFromCurrencyISOCode(String fromCurrencyISOCode) {
        this.fromCurrencyISOCode = fromCurrencyISOCode;
    }

    public void setToCurrencyISOCode(String toCurrencyISOCode) {
        this.toCurrencyISOCode = toCurrencyISOCode;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

}
