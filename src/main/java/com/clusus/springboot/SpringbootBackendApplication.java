package com.clusus.springboot;

import com.clusus.springboot.repository.DealRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by anuj on 12/16/22.
 */

@SpringBootApplication
public class SpringbootBackendApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootBackendApplication.class, args);
    }

    @Autowired
    private DealRepository dealRepository;

    @Override
    public void run(String... args) throws Exception {
//		Deal deal = new Deal();
//        deal.setUniqueId("hi");
//        deal.setFromCurrencyISOCode("a");
//        deal.setToCurrencyISOCode("b");
//        deal.setTimestamp( new Date());
//        deal.setAmount(5000.00);
//        dealRepository.save(deal);
    }
}