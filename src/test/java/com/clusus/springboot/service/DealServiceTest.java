package com.clusus.springboot.service;

import com.clusus.springboot.model.Deal;
import java.util.Date;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DealServiceTest {

    @Autowired
    DealService dealService;

    @Test
    void testAddDeal() throws Exception{

        Deal deal = getDeal();
        Assertions.assertNotNull(dealService.saveDeal(getJSONObject()));
    }

    @Test
    void testDealExists() throws Exception {
        dealService.saveDeal(getJSONObject());
        Assertions.assertTrue(dealService.existsByUniqueId("rrr"));
    }

    @Test
    void testDealDoesNotExist(){
        Assertions.assertFalse(dealService.existsByUniqueId("test"));
    }

    @Test
    void testValidUniqueId(){
        Assertions.assertTrue(dealService.isValidUniqueId("test"));
    }

    @Test
    void testInvalidUniqueId() throws Exception {
        dealService.saveDeal(getJSONObject());
        Assertions.assertFalse(dealService.isValidUniqueId("rrr"));
    }

    @Test
    void testNullUniqueId() throws Exception {
        dealService.saveDeal(getJSONObject());
        Assertions.assertFalse(dealService.isValidUniqueId(null));
    }

    @Test
    void testValidDate() throws Exception {
        Assertions.assertTrue(dealService.isValidDate("166062313900"));
    }

    @Test
    void testInvalidDate() throws Exception {
        Assertions.assertFalse(dealService.isValidDate("20-20-20"));
    }

    @Test
    void testNullDate() throws Exception {
        Assertions.assertFalse(dealService.isValidDate(null));
    }

    @Test
    void testValidAmount() throws Exception {
        Assertions.assertTrue(dealService.isValidAmount("6474671"));
    }

    @Test
    void testInvalidAmount() throws Exception {
        Assertions.assertFalse(dealService.isValidAmount("647TY"));
    }


    private Deal getDeal() {
        Deal deal = new Deal();
        deal.setUniqueId("test");
        deal.setFromCurrencyISOCode("USD");
        deal.setToCurrencyISOCode("NPR");
        deal.setTimestamp(new Date());
        deal.setAmount(100D);
        return deal;
    }

    private JSONObject getJSONObject() throws Exception{
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("toCurrencyISOCode","b");
        jsonObject.put("amount","6474671");
        jsonObject.put("fromCurrencyISOCode","a");
        jsonObject.put("uniqueId","rrr");
        jsonObject.put("timestamp","166062313900");
        return jsonObject;
    }
}
