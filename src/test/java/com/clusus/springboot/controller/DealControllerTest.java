package com.clusus.springboot.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.clusus.springboot.service.DealService;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * Created by anuj on 12/16/22.
 */



@ExtendWith(SpringExtension.class)
public class DealControllerTest {

    private MockMvc mockMvc;

    @Mock
    DealService dealService;

    @InjectMocks
    private DealController dealController;

    @BeforeEach
    public void setUp(){
        mockMvc = MockMvcBuilders.standaloneSetup(dealController).build();
    }

    @Test
    public void createDeal_SuccessfulTest() throws Exception{
        when(dealService.saveDeal(any())).thenReturn(new ResponseEntity<String>("Success", HttpStatus.OK));
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/deal").content(getJSONObject().toString()).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is2xxSuccessful());
    }

    @Test
    public void createDeal_InternalServerErrorTest() throws Exception{

        when(dealService.saveDeal(any())).thenReturn(new ResponseEntity<>("Invalid date", HttpStatus.INTERNAL_SERVER_ERROR));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/deal").content(getJSONObject().toString()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is5xxServerError());

    }

    private JSONObject getJSONObject() throws Exception{
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("toCurrencyISOCode","b");
        jsonObject.put("amount","6474671");
        jsonObject.put("fromCurrencyISOCode","a");
        jsonObject.put("uniqueId","rrr");
        jsonObject.put("timestamp","166062313900");
        return jsonObject;
    }



}