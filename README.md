#CLUSUS ASSIGNMENT

Assignment for Clusus

* Takes a request for deal and saves it in the database.

##FEATURES

* Takes Request Fields(Deal Unique Id, From Currency ISO Code "Ordering Currency", To Currency ISO Code, Deal timestamp, Deal Amount in ordering currency).
* Validates row structure.(e.g: Missing fields, Type format..etc. We do not expect you to cover all possible cases but we'll look to how you'll implement validations)
* System does not import same request twice.
* No rollback allowed, what every rows imported is saved in DB.

###STARTUP

* Docker-compose up --build to build and start the docker container.
* Use postman to insert data through JSON objects.
* Get request http://localhost:8080/api/v1/deal/

###SAMPLE REQUEST

* {
  "uniqueId":"rrr11",
  "fromCurrencyISOCode": "a",
  "toCurrencyISOCode": "b",
  "timestamp": "166062313900",
  "amount": "647467"
  }